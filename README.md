# REPO FOR SHARED NOTEBOOKS

this repo is supposed to be used as an environment to be able to work/execute online a jupyterlab application (for example for ipython notebooks).

* Open the https address of this repo in a binder, provided by mpcdf, at:
  [https://notebooks.mpcdf.mpg.de/binder/](https://notebooks.mpcdf.mpg.de/binder/)

* Once the binder runs, the jupyterlab has a git interface. Also the git is configured globally to strip the output and metadata of the `ipynb` files before commit (a filter).

* In the Jupyterlab, you can open a terminal, and it should be possible to `git clone` the actual repositories with the files you want to work with.

 

# Setting up a virtual environment

It is usefull to install a virtual environment and make it usable by the jupyterlab on binder.

Once on the Jupyterlab, you can open a terminal and make a new virtual environment:
```
python -m venv myenv
```
it creates the folder `myenv`. Then you can activate it and install the requirement packages there:
```
source myenv/bin/activate
pip install scipy
pip install netCDF4==1.5.7 
...
```
and to be able to choose the environment for the ipython notebooks, you do:
```
pip install ipykernel
python -m ipykernel install --user --name myenv
```

# More info
This is an extension from the one of  Frank Berghaus, `fberg/my-first-binder`.

The idea is to separate environment and content, so this repo should not be used for storing notebooks, since every commit make binder to create a new docker image at start-up.
see https://discourse.jupyter.org/t/tip-speed-up-binder-launches-by-pulling-github-content-in-a-binder-link-with-nbgitpuller/922


The BinderHub/JupyterHub community also have extensive documentation, a good entry point is here: 
https://mybinder.readthedocs.io/en/latest/using/using.html#preparing-a-repository-for-binder	